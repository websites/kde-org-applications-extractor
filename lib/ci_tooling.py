# SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import logging

from yaml import safe_load

from lib.fnmatch_pattern import FNMatchPattern


class RepositoryGroup:
    def __init__(self, cfg: dict):
        logging.info(cfg)
        self.repositories = [FNMatchPattern(x) for x in cfg['repositories']]
        self.platforms = set(cfg['platforms'])


class Product:
    def __init__(self, name, data: dict):
        self.name = name
        self.branch_groups = set(data['branchGroups'])
        self.includes = [RepositoryGroup(x) for x in data['includes']]

    def __str__(self):
        return self.name

    @classmethod
    def list(cls):
        with open('ci-tooling/local-metadata/product-definitions.yaml') as f_def:
            data = safe_load(f_def)
        return [Product(key, value) for key, value in data.items()]


class RepositoryFilter:
    def __init__(self, branch_groups, platforms):
        self.branch_groups = set(branch_groups)
        self.platforms = set(platforms)

    def filter(self, product: Product):
        if not self.branch_groups.issubset(product.branch_groups):
            return []
        repos = []
        for repo_group in product.includes:
            if not self.platforms.issubset(repo_group.platforms):
                continue
            repos.extend(repo_group.repositories)
        return list(set(repos))
