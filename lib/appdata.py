# SPDX-FileCopyrightText: 2017-2018 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

from lib.py_appstream.component import Component


class AppData:
    def __init__(self, path):
        self.path = path

    def read_as_yaml(self):
        with open(self.path) as f:
            data = f.read()
        appdata = Component()
        appdata.parse_tree(data)
        return appdata.serialize()
