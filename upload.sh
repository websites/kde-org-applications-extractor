#!/bin/bash

set -xe

cd ..
rm -rf appdata-extensions; cp -r kde-org-applications-extractor/appdata-extensions .
tar zcf applications.tar.gz appdata appdata-extensions icons thumbnails index.json
scp applications.tar.gz embra:/home/jr/www/www.kde.org/applications/
ssh embra 'cd /home/jr/www/www.kde.org/applications; tar xf applications.tar.gz; rm applications.tar.gz'
rm applications.tar.gz
cd -
