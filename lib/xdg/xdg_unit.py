# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later
from __future__ import annotations

import os


class XDGUnit:
    _xdg_data_dirs: list[str] = []
    subdir = ''

    @classmethod
    def xdg_data_dirs(cls):
        if not cls._xdg_data_dirs:
            dirs = os.environ.get('XDG_DATA_HOME', '~/.local/share')
            dirs += ':' + os.environ.get('XDG_DATA_DIRS', '/usr/local/share/:/usr/share/')
            paths = [os.path.join(os.path.expanduser(path), cls.subdir) for path in dirs.split(':') if '..' not in path]
            cls._xdg_data_dirs = list(set(paths))
        return cls._xdg_data_dirs
