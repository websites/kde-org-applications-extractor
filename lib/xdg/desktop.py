# SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

from __future__ import annotations

import configparser
import logging
import os
import re

from lib.xdg.xdg_unit import XDGUnit


class Desktop:
    def __init__(self, path):
        self.path = path
        # RawConfigParser doesn't do string interpolation
        cp = configparser.RawConfigParser()
        # By default, keys are lowercased. This makes it keep the case
        cp.optionxform = str
        cp.read(path)
        self.desktop_config = cp['Desktop Entry']
        self.icon = self.desktop_config.get('Icon', '')  # not all desktop files have 'Icon'
        if 'Categories' in self.desktop_config:
            self.categories = self.desktop_config['Categories'].split(';')
        else:
            self.categories = []

    @staticmethod
    def _listify(string):
        return [] if not string else list(set([s for s in string.split(';') if s]))

    def only_show_in(self):
        return self._listify(self.desktop_config.get('OnlyShowIn', ''))

    def not_show_in(self):
        return self._listify(self.desktop_config.get('NotShowIn', ''))

    def show_in(self, desktop):
        # Spec makes this easier for us:
        # > The same desktop name may not appear in both OnlyShowIn and NotShowIn of a group.
        # If it is explicitly not shown do that
        if desktop in self.not_show_in():
            return False
        # If only_show_in is set we must be in there
        if self.only_show_in():
            return desktop in self.only_show_in()
        # Else only to be shown when 'OnlyShowIn' is not present
        return 'OnlyShowIn' not in self.desktop_config

    def localized(self, key) -> dict[str, str]:
        # returns dict of key/value pairs of all localized values; the master
        # key gets represented as 'C'
        options = {}
        expression = re.compile(rf'{key}($|\[.+])')
        for entry_key, entry_val in self.desktop_config.items():
            match = expression.match(entry_key)
            if match:
                lang = 'C' if entry_key == key else match.group(1)[1:-1]
                options[lang] = entry_val
        return options


class DesktopFileLoaderBase(XDGUnit):
    subdir = 'applications'
    file_ext = '.desktop'

    def __init__(self, app_id, extra_data_dirs=None):
        if extra_data_dirs is None:
            extra_data_dirs = []
        self.id = app_id if app_id.endswith(self.file_ext) else app_id + self.file_ext
        self.extra_data_dirs = extra_data_dirs

    def find(self):
        path = self.find_by_id(self.id)
        return Desktop(path) if path else None

    def find_by_id(self, d_id: str):
        for d in self._data_dirs():
            # a trick to prevent scripty from processing desktop files in 'appdata-unmaintained' folder
            if d == 'appdata-unmaintained':
                d_id = d_id[:-1]
            f = os.path.join(d, d_id)
            if os.path.isfile(f):
                logging.info(f'\'{d_id}\' match: \'{f}\'')
                return f
        return ''

    def _data_dirs(self):
        return self.extra_data_dirs + self.xdg_data_dirs()


class DesktopDirectoryLoader(DesktopFileLoaderBase):
    subdir = 'desktop-directories'
    file_ext = '.directory'


class ApplicationDesktopLoader(DesktopFileLoaderBase):
    subdir = 'applications'
    file_ext = '.desktop'


if __name__ == "__main__":
    app_desktop = ApplicationDesktopLoader('org.kde.picmi').find()
    print(app_desktop.path, app_desktop.icon, app_desktop.categories)
    desktop_directory = DesktopDirectoryLoader('kf5-games').find()
    print(desktop_directory.path, desktop_directory.icon)
