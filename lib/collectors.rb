# Copyright 2017-2018 Harld Sitter <sitter@kde.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License or (at your option) version 3 or any later version
# accepted by the membership of KDE e.V. (or its successor approved
# by the membership of KDE e.V.), which shall act as a proxy
# defined in Section 14 of version 3 of the license.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'concurrent'
require 'tty/command'
require 'pp'
require 'json'
require 'open-uri'
require 'tmpdir'

require_relative 'app_data'
require_relative 'category'
require_relative 'ci_tooling'
require_relative 'icon_fetcher'
require_relative 'kde_project'
require_relative 'thumbnailer'
require_relative 'xml_languages'

require_relative 'xdg/desktop'
require_relative 'xdg/icon'

class InvalidError < StandardError; end

class AppStreamCollector
  APPLICATION_TYPES = %w[desktop desktop-application console-application
                         web-application addon]

  attr_reader :dir
  attr_reader :path
  attr_reader :project

  attr_reader :appdata
  attr_reader :appid

  attr_reader :tmpdir

  def initialize(dir, path:, project:, theme:)
    @dir = dir
    @path = path
    @project = project
    @appdata = AppData.new(path).read_as_yaml
    # Sanitize for our purposes (can be either foo.desktop or foo, we always
    # use the latter internally).
    # The value in the appdata must stay as it is or appstream:// urls do not
    # work!
    @appid = appdata['ID'].gsub('.desktop', '')
    # WARNING do not mutate ID in appdata!
    # libappstream is too daft to handle foo.desktop and foo the same, so for
    # external purposes we need to keep the input ID. This is particularly
    # important as to not break expectations when handling our data blobs e.g.
    # to build appstream:// URIs in the frontend code.
    # Instead we'll pack our mutated id into a special key we can later use
    # to also uniquely identify things in the frontend.
    appdata['X-KDE-ID'] = @appid
    @icon_theme = theme
  end

  def xdg_data_dir
    "#{dir}/share/"
  end

  def desktop_file
    @desktop_file ||=
      XDG::ApplicationsLoader.new(appid, extra_data_dirs: ["#{xdg_data_dir}/applications", "appdata-unmaintained"]).find
  end

  def theme
    @icon_theme ||=
      XDG::IconTheme.new('breeze', extra_data_dirs: ["#{xdg_data_dir}/icons"])
  end

  # is type desktop or desktop-application
  def is_desktop_app
    return appdata['Type'][0..6] == 'desktop'
  end

  # FIXME: overridden for git crawling
  def grab_icon
    unmaintained = JSON.load(File.open("unmaintained.json"))
    if !is_desktop_app
      if appdata.include? 'Icon' and appdata['Icon'].include? 'stock'
        iconname = appdata['Icon']['stock']
      else
        appdata.fetch('Icon', 'planetkde')
      end
    elsif unmaintained.include?(File.basename(project.id))
      iconname = 'planetkde' # a generic icon
    else
      iconname = desktop_file.icon
    end
    return unless iconname
    IconFetcher.new(iconname, theme).extend_appdata(appdata, cachename: appid)
  end

  def grab_categories
    if is_desktop_app
      desktop_categories = desktop_file.categories & MAIN_CATEGORIES
      unless desktop_categories
        # FIXME: record into log
        raise InvalidError, "#{appid} has no main categories. only has #{desktop_categories}"
      end
    end

    appdata['Categories'] ||= []
    # Special case khelpcenter which is in Core category
    if @appid == 'org.kde.Help'
      appdata['Categories'] = ['System']
    end
    # Iff the categories were defined in the appdata as well make sure to
    # filter all !main categories.
    appdata['Categories'] = appdata['Categories'] & MAIN_CATEGORIES
    if is_desktop_app
      appdata['Categories'] += desktop_categories
    end
    appdata['Categories'].uniq!
    appdata['Categories'].collect! { |x| Category.to_name(x) }
  end

  def grab_generic_name
    if is_desktop_app
      appdata['X-KDE-GenericName'] =
        XMLLanguages.from_desktop_entry(desktop_file, 'GenericName')
    else
      appdata['X-KDE-GenericName'] = appdata['Summary']
    end
  end

  def grab_project
    appdata['X-KDE-Project'] = project.id
    appdata['X-KDE-Repository'] = project.repo
  end

  def grab
    unless APPLICATION_TYPES.include?(appdata.fetch('Type', 'generic'))
      warn "#{appid} is not an application"
      return false
    end
    # kdeconnect claims its a desktop app but it really isn't
    if appdata['ID'] == 'org.kde.kdeconnect.kcm.desktop'
        appdata['Type'] = 'addon'
    end
    # addons have no desktop files
    if is_desktop_app
      raise InvalidError, "no desktop file for #{appid}" unless desktop_file
      unless desktop_file.show_in?('KDE') && desktop_file.display? && !desktop_file.hidden?
      raise InvalidError, "desktop file for #{appid} not meant for display"
      end
    end

    puts "Now proccessing ID: #{appdata['ID']}"
    # FIXME: thumbnailer should not mangle the appdata, it should generate
    #   the thumbnails and we do the mangling...
    Thumbnailer.thumbnail!(appdata)
    grab_icon
    grab_categories
    grab_generic_name
    grab_project

    FileUtils.mkpath('../appdata', verbose: true)
    # File.write("../appdata/#{appdata.fetch('ID').gsub('.desktop', '')}.yaml", YAML.dump(appdata))
    File.write("../appdata/#{appdata.fetch('ID').gsub('.desktop', '')}.json", JSON.pretty_generate(appdata))

    # FIXME: we should put EVERYTHING into a well defined tree in a tmpdir,
    #   then move it into place in ONE place. so we can easily change where
    #   stuff ends up in the end and know where it is while we are working on
    #   the data
    true
  end

  def self.grab(dir, project:, theme: nil)
    # FIXME: the return value is no good. we need to differentiate:
    #    found no appdata from no good appda from good appdata
    #    former would need subsequent collectors run, latter simply means the
    #    project contains nothing worthwhile
    unmaintained = JSON.load(File.open("unmaintained.json"))
    if unmaintained.include?(File.basename(project.id))
      id = "org.kde." + File.basename(project.id)
      dirs = Dir.glob("appdata-unmaintained/#{id}.appdata.xml")
    else
      # metainfo.xml is the right extension to use unless you're an
      # app and don't want to use that one in which case use appdata.xml
      dirs = Dir.glob("#{dir}/**/**.appdata.xml")
      dirs += Dir.glob("#{dir}/**/**.metainfo.xml")
    end
    dirs.select! do |x|
      not x.match('org.example')
    end
    any_good = false
    dirs.select! do |x|
      not x.match('org.example')
    end
    dirs.each do |path|
      warn "  Grabbing #{path}"
      begin
        good = new(dir, path: path, project: project, theme: theme).grab
        any_good ||= good
      rescue InvalidError => e
        warn e
      end
    end
    any_good
  end
end

class GitAppStreamCollector < AppStreamCollector
  def xdg_data_dir
    "#{Dir.pwd}/breeze-icons/share/"
  end

  # FIXME: deferring to appstream via xdg_data_dir
  # def grab_icon
  #   raise 'not implemented'
  #   # a) should look in breeze-icon unpack via theme
  #   # b) should try to find in tree?
  #   # c) maybe an override system?
  # end

  def desktop_file
    unmaintained = JSON.load(File.open("unmaintained.json"))
    if unmaintained.include?(File.basename(project.id))
      id = "org.kde." + File.basename(project.id)
      files = Dir.glob("appdata-unmaintained/#{id}.desktop")
    else
      files = Dir.glob("#{dir}/**/#{appid}.desktop{.in,}")
    end
    files.select! do |x|
      not x.match('snap/setup')
    end
    files.select! do |x|
      not x.match('snap/gui')
    end
    files.select! do |x|
      not x.match('APPNAMELC') and not x.match('org.example')
    end
    raise "#{appid}.desktop: #{files.inspect}" unless files.size == 1
    puts "found desktop at #{files[0]}"
    XDG::Desktop.new(files[0])
  end
end
