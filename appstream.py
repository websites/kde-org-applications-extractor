# SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import concurrent.futures
import json
import logging
import os
import shutil
import subprocess
import tempfile

from lib import category
from lib.ci_tooling import Product, RepositoryFilter
from lib.collectors import AppstreamCollector, GitAppstreamCollector
from lib.fnmatch_pattern import FNMatchPattern
from lib.icon_fetcher import IconFetcher
from lib.kde_project import Project
from lib.xdg.desktop import DesktopDirectoryLoader
from lib.xdg.icon import IconTheme

if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

    if 'XDG_DATA_DIRS' not in os.environ or not os.environ['XDG_DATA_DIRS']:
        os.environ['XDG_DATA_DIRS'] = '/usr/local/share:/usr/share'

    __dir__ = os.path.dirname(os.path.abspath(__file__))
    if os.getcwd() != __dir__:
        os.chdir(__dir__)

    if not os.path.isdir('ci-tooling'):
        subprocess.run(['git', 'clone', '--depth', '1',
                        'https://invent.kde.org/sysadmin/ci-tooling.git'])
    if not os.path.isdir('ci-tooling/kde-build-metadata'):
        subprocess.run(['git', 'clone', '--depth', '1',
                        'https://invent.kde.org/sysadmin/kde-build-metadata.git'],
                       cwd='ci-tooling/')
    if not os.path.isdir('ci-tooling/repo-metadata'):
        subprocess.run(['git', 'clone', '--depth', '1',
                        'https://invent.kde.org/sysadmin/repo-metadata.git'],
                       cwd='ci-tooling/')

    branch_group = 'kf5-qt5'
    platform_blacklist = [
        FNMatchPattern('*Windows*'),
        FNMatchPattern('*Android*'),
        FNMatchPattern('*BSD*')
    ]

    with open('unmaintained.json') as f_u:
        unmaintained = json.load(f_u)
    with open('unmaintained-no-repo.json') as f_unr:
        unmaintained_no_repo = json.load(f_unr)

    products = Product.list()

    # Keep a pristine list without applying filtering. We'll need to make
    # unopinionated checks against "known" projects regardless of their
    # applicability
    all_projects = Project.list()
    for old_app in unmaintained_no_repo:
        all_projects.append(f'kde/old/{old_app}')
    # filtered projects
    projects = []

    # paths starting with one of these are excluded
    # kde/kdebindings: unmaintained stuff
    # kdesupport: the frameworks of the 90's
    starting_strings_non_apps = ['books/', 'documentation/', 'extragear/libs/', 'frameworks/', 'historical/',
                                 'kde/kdebindings', 'kdesupport/', 'others', 'sysadmin/', 'websites/']
    # specific apps in each group
    workspace_apps = ['discover', 'plasma-systemmonitor', 'ksysguard']
    kdereview_apps = ['kclock', 'subtitlecomposer']
    playground_apps = ['index-fm', 'kaidan', 'kirogi', 'krecorder', 'maui-pix', 'nota',
                       'plasmatube', 'qmlkonsole',
                       'tokodon', 'vakzination', 'vvave']
    # individual non-apps
    plasmoids = ['alkimia', 'latte-dock']
    non_apps = ['gcompris-data'] + plasmoids
    # for testing
    testing_apps = ['kst-plot', 'kdebugsettings', 'konqueror'] + workspace_apps + kdereview_apps + playground_apps

    # filter all_projects
    for project in all_projects:
        # some groups are excluded altogether
        if any(map(lambda x: project.startswith(x), starting_strings_non_apps)):
            continue
        # Plasma has its own web page, include only some projects
        if project.startswith('kde/workspace/'):
            if os.path.basename(project) not in workspace_apps:
                continue
        # Apps in review aren't ready for the public.
        if project.startswith('kdereview/'):
            if os.path.basename(project) not in kdereview_apps:
                continue
        # Playground stuff aren't ready for the public.
        if project.startswith('playground/'):
            if os.path.basename(project) not in playground_apps:
                continue
        # We do want some unmaintained bits for backwards compatibility
        # hidden away at https://kde.org/applications/unmaintained/
        if project.startswith('unmaintained/'):
            if os.path.basename(project) not in unmaintained:
                continue
        # individual non-apps
        if os.path.basename(project) in non_apps:
            continue
        # if os.path.basename(project) not in testing_apps:
        #     continue
        # other than those, include all
        projects.append(project)

    # Artifacts are tarred into products, since this is grouping stuffed on top of
    # project metadata we'll simply try and ignore failure. Otherwise this gets
    # over-engineered quickly.
    # FIXME: needs to raise some sort of error on products which have no appdata!

    dep_file = f'ci-tooling/repo-metadata/dependencies/dependency-data-{branch_group}'
    with open(dep_file, 'w') as f_dep:  # truncate
        pass

    # Download breeze-icons for getting icons and plasma-workspace for getting app menu categories
    SUPPORTS = {
        'breeze-icons': {
            'product': 'Frameworks',
            'project': 'frameworks/breeze-icons'
        },
        'plasma-workspace': {
            'product': 'Plasma',
            'project': 'kde/workspace/plasma-workspace'
        }
    }
    SUPPORTS_PLATFORM = 'SUSEQt5.15'

    with tempfile.TemporaryDirectory() as d_tmp:
        for basename in SUPPORTS:
            if os.path.isdir(basename):
                continue
            project = SUPPORTS[basename]['project']
            dep_project = f'{project}-appstream'
            with open(dep_file, 'a') as f_dep:
                f_dep.write(f'{dep_project}: {project}\n')
            proj_tmp_dir = os.path.join(d_tmp, project)
            os.makedirs(proj_tmp_dir, exist_ok=True)
            command_parts = ['python3', '-u', 'ci-tooling/helpers/prepare-dependencies.py',
                             '--product', SUPPORTS[basename]['product'],
                             '--project', dep_project,
                             '--branchGroup', branch_group,
                             '--environment', 'production',
                             '--platform', SUPPORTS_PLATFORM,
                             '--installTo', proj_tmp_dir]
            logging.info(f'{command_parts}')
            res = subprocess.call(command_parts)
            if res == 0:
                # There are symlinks in projects
                shutil.copytree(proj_tmp_dir, basename, symlinks=True)

    if not os.path.isdir('breeze-icons'):
        raise Exception('Failed to get breeze-icons! Resolution of CI artifacts failed!')
    if not os.path.isdir('plasma-workspace'):
        raise Exception('Failed to get plasma-workspace! Resolution of CI artifacts failed!')

    for cat, desktop_id in {**category.CATEGORY_DESKTOPS_MAP, **category.SUBCATEGORY_DESKTOPS_MAP}.items():
        loader = DesktopDirectoryLoader(desktop_id,
                                        [os.path.join(__dir__, 'plasma-workspace/share/desktop-directories')])
        desktop = loader.find()
        if not desktop:
            raise Exception(f'No desktop file for {desktop_id}')
        theme = IconTheme('breeze', [os.path.join(__dir__, 'breeze-icons/share/icons')])
        name = category.to_code(cat)
        icon_name = desktop.desktop_config['Icon']
        IconFetcher(icon_name, theme).extend_appdata({}, name, 'categories')

    # Download compiled apps from build.kde.org CI
    with tempfile.TemporaryDirectory() as d_tmp:
        def process_app(a_project, a_product, a_platform, a_dep_project, a_proj_tmp_dir):
            a_command_parts = ['python3', '-u', 'ci-tooling/helpers/prepare-dependencies.py',
                               '--product', str(a_product),
                               '--project', a_dep_project,
                               '--branchGroup', branch_group,
                               '--environment', 'production',
                               '--platform', a_platform,
                               '--installTo', a_proj_tmp_dir]
            logging.info(f'{a_command_parts}')
            if subprocess.call(a_command_parts) != 0:
                raise Exception
            logging.info(f'processing {a_proj_tmp_dir}')
            # FIXME: !!! by only removing the project if it grabbed we run the prepare
            #   multiple times if multiple platforms are available even though
            #   we have found an install, we just didn't find any data.
            #   this needs a second list kept of projects we looked at it already.
            #   the projects array is the list of projects we found something for
            #   so git has a shrunk list. we still want git to run on projects
            #   that had a tree but no data though.
            icon_theme = IconTheme('breeze', [f'{__dir__}/breeze-icons/share/icons'])
            if AppstreamCollector.grab_data(a_proj_tmp_dir, Project.get(a_project), icon_theme):
                projects.remove(a_project)
            shutil.rmtree(a_proj_tmp_dir)

        project_args = {}
        for product in products:
            # Aggregate all platforms so we can iterate on them.
            platforms = []
            for include in product.includes:
                platforms += include.platforms
            platforms = list(set(platforms))
            # Only process supported ones
            for platform in filter(lambda x: x not in platform_blacklist, platforms):
                repo_filter = RepositoryFilter([branch_group], [platform])
                projects_in_product = repo_filter.filter(product)
                for project in projects:
                    if project not in projects_in_product:
                        continue
                    # NB: the basename of the project must be different or the dep resolver
                    #   will think it is a looping dep (even when it is in a different scope
                    #   e.g. appstream/)
                    dep_project = f'{project}-appstream'
                    proj_tmp_dir = os.path.join(d_tmp, project)

                    with open(dep_file, 'a') as f_dep:
                        f_dep.write(f'{dep_project}: {project}\n')
                    logging.info(f'mangling {proj_tmp_dir}')
                    os.makedirs(proj_tmp_dir, exist_ok=True)

                    project_args[project] = (project, product, platform, dep_project, proj_tmp_dir)

        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
            # Mark each future with its project
            future_to_project = {executor.submit(process_app, *project_args[p]): p for p in project_args}
            for future in concurrent.futures.as_completed(future_to_project):
                project = future_to_project[future]
                try:
                    future.result()
                except Exception as e:
                    logging.warning('Project %r generated an exception: %s' % (project, e))
                else:
                    logging.info(f'{project} processed')

    logging.info(f'Left: {projects}')

    unprocessed = []
    with tempfile.TemporaryDirectory() as d_tmp:
        for project_id in projects:
            project = Project.get(project_id)
            proj_tmp_dir = os.path.join(d_tmp, project.id)
            if os.path.basename(project_id) not in unmaintained_no_repo:
                command_parts = ['git', 'clone', '--depth', '1',
                                 f'https://invent.kde.org/{project.repo}.git', proj_tmp_dir]
                logging.info(f'{command_parts}')
                subprocess.run(command_parts)
            if not GitAppstreamCollector.grab_data(proj_tmp_dir, project):
                unprocessed.append(project_id)
                shutil.rmtree(proj_tmp_dir, ignore_errors=True)
    projects = unprocessed

    logging.info(f'Not processed: {projects}')
