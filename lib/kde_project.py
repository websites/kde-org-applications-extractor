# SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

import concurrent.futures
import json
import logging
import os.path

import requests


class ProjectMeta(type):
    _cache = {}
    with open('unmaintained-no-repo.json') as f_unr:
        _unmaintained_no_repo = json.load(f_unr)

    @property
    def cache(cls):
        return cls._cache

    @cache.setter
    def cache(cls, value):
        cls._cache = value

    @property
    def unmaintained_no_repo(cls):
        return cls._unmaintained_no_repo

    def get(cls, path):
        # Retrieve a single project and report the path and contents
        # get from cache if applicable
        project = cls._from_cache(path)
        if project:
            return project

        data = cls._override(path)
        if not data:
            data = requests.get(f'https://projects.kde.org/api/v1/project/{path}').json()
        return Project(path, data)

    def list(cls) -> list:
        logging.info('- Resolving all projects to support legacy project paths -')
        logging.info('This will take a while...')
        data = requests.get('https://projects.kde.org/api/v1/projects')
        cls.cache = cls._hash_paths(data.json())
        # The keys are the ids (aka path) and is faked to be the legacy
        # project path, so we return the list of keys here rather than the
        # list obtained via the /projects endpoint
        return list(cls.cache.keys())

    def _hash_paths(cls, paths) -> dict:
        # rejigger everything to use the legacy path instead as ci-tooling
        # still uses them rather than the actual project paths. So we resolve
        # all projects right now and pretend like they all use legacy paths.
        result = {}
        # We can use a with statement to ensure threads are cleaned up promptly.
        # We can be somewhat aggressive here because the data is aggressively
        # cached on the server side, so requests take microseconds on the
        # server side.
        # Also, we don't do much client-side so the higher the concurrency
        # the better, even on systems with very few cores.
        with concurrent.futures.ThreadPoolExecutor(max_workers=256) as executor:
            # Start the load operations and mark each future with its path
            future_to_path = {executor.submit(cls.get, path): path for path in paths}
            for future in concurrent.futures.as_completed(future_to_path):
                path = future_to_path[future]
                try:
                    project = future.result()
                except Exception as exc:
                    logging.warning('Path %r generated an exception: %s' % (path, exc))
                else:
                    result[project.id] = project
        return result

    def _from_cache(cls, path):
        if path not in cls.cache:
            return {}
        return cls.cache[path]

    def _override(cls, path):
        old_app = os.path.basename(path)
        if old_app not in cls.unmaintained_no_repo:
            return None

        logging.info(f'Overriding {path} with an old blob!')
        return {
            'repo': old_app,
            '__do_not_use-legacy-projectpath': f'kde/pim/{old_app}'
        }


class Project(metaclass=ProjectMeta):
    def __init__(self, path, obj):
        # Hijack and pretend this project still uses the legacy path
        try:
            self.id = obj['__do_not_use-legacy-projectpath']
            self.repo = obj['repo']
        except KeyError as e:
            raise KeyError(f'{path} :: {obj.repr()} :: {e}')
