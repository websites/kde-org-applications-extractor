# frozen_string_literal: true
#
# Copyright (C) 2017-2020 Harald Sitter <sitter@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

require 'open-uri'

module KDE
  # A project definition.
  class Project
    class << self
      def get(path)
        # get from cache if applicable
        object = from_cache(path)
        return object if object

        data = override(path)
        data ||= URI.open("https://projects.kde.org/api/v1/project/#{path}").read
        new(path, JSON.parse(data))
      end

      def list
        warn '- Resolving all projects tp support legacy project paths -'
        warn 'This will take a while...'
        data = URI.open('https://projects.kde.org/api/v1/projects').read
        @cache = hash_paths(JSON.parse(data))
        # The keys are the ids (aka path) and is faked to be the legacy
        # project path, so we return the list of keys here rather than the
        # list obtained via the /projects endpoint
        @cache.keys
      end

      private

      def hash_paths(paths)
        # rejigger everything to use the legacy path instead as ci-tooling
        # still uses them rather than the actual project paths. So we resolve
        # all projects right now and pretend like they all use legacy paths.
        # NB: also in #initialize
        Thread.abort_on_exception = true
        promises = paths.collect do |path|
          Concurrent::Promise.execute(executor: promise_executor) do
            get(path)
          end
        end
        hash = promises.collect(&:value!)
        hash.collect { |project| [project.id, project] }.to_h
      end

      def unmaintained_no_repo
        # rubocop:disable Security/JSONLoad
        # we load a trusted file here
        @unmaintained_no_repo ||=
          JSON.load(File.open('unmaintained-no-repo.json'))
        # rubocop:enable Security/JSONLoad
      end

      def old_blob_for(oldapp)
        # This only includes values we need later
        {
          repo: oldapp,
          # rubocop:disable Style/HashSyntax
          :'__do_not_use-legacy-projectpath' => "kde/pim/#{oldapp}"
          # rubocop:enable Style/HashSyntax
        }
      end

      def override(path)
        oldapp = File.basename(path)
        return nil unless unmaintained_no_repo.include?(oldapp)

        warn "Overriding #{path} with an old blob!"
        JSON.generate(old_blob_for(oldapp))
      end

      def promise_executor
        @promise_executor ||=
          Concurrent::ThreadPoolExecutor.new(
            min_threads: 1,
            # We can be somewhat aggressive here because the data is aggresively
            # cached on the server side, so requests take microseconds on the
            # server side.
            # Also we don't do much client-side so the higher the concurrency
            # the better, even on systems with very few cores.
            max_threads: 256,
            fallback_policy: :caller_runs
          )
      end

      def from_cache(path)
        (@cache ||= {})[path]
      end
    end

    attr_reader :id
    attr_reader :repo

    def initialize(path, hash)
      # Hijack and pretend this project still uses the legacy path
      # NB: also see #list
      # Use fetch so this very aggresively explodes if, or rather when, the
      # attribute gets removed from the API again.
      @id = hash.fetch('__do_not_use-legacy-projectpath')
      @repo = hash.fetch('repo')
    rescue KeyError => e
      raise KeyError, "#{path} :: #{hash.inspect} :: #{e.message}"
    end
  end
end
