# SPDX-FileCopyrightText: 2017 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

from lib.xdg.desktop import Desktop


class XMLLanguages:
    trans_table = str.maketrans('@_', '--')

    @classmethod
    def from_desktop_entry(cls, desktop: Desktop, key):
        names = {}
        for lang, value in desktop.localized(key).items():
            # XML language notation is hyphen for everything pretty much.
            k = lang.translate(cls.trans_table)
            names[k] = value
        return names
