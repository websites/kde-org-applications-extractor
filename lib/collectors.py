# SPDX-FileCopyrightText: 2017-2018 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import glob
import json
import logging
import os

from lib import category
from lib.appdata import AppData
from lib.icon_fetcher import IconFetcher
from lib.kde_project import Project
from lib.xdg.desktop import ApplicationDesktopLoader, Desktop
from lib.xdg.icon import IconTheme
from lib.xml_languages import XMLLanguages


class CollectorException(Exception):
    pass


class AppstreamCollector:
    APPLICATION_TYPES = ['desktop', 'desktop-application', 'console-application',
                         'web-application', 'addon']
    with open('unmaintained.json') as f_u:
        _unmaintained = json.load(f_u)

    def __init__(self, directory, path, project: Project, theme):
        self.directory = directory
        self.path = path
        self.project = project
        self.appdata = AppData(self.path).read_as_yaml()
        # Sanitize for our purposes (can be either foo.desktop or foo, we always use the latter internally).
        # The value in the appdata must stay as it is or appstream:// urls do not work!
        self.appid = self.appdata['ID'].replace('.desktop', '')
        # WARNING do not mutate ID in appdata!
        # libappstream is too daft to handle foo.desktop and foo the same, so for external purposes we need
        # to keep the input ID. This is particularly important as to not break expectations when handling
        # our data blobs e.g. to build appstream:// URIs in the frontend code. Instead, we'll pack our
        # mutated id into a special key we can later use to also uniquely identify things in the frontend.
        self.appdata['X-KDE-ID'] = self.appid
        self.icon_theme = theme
        self.desktop_file = None

    def xdg_data_dir(self):
        return f'{self.directory}/share'

    def get_desktop_file(self):
        if not self.desktop_file:
            # Special case: kdeconnect
            appid = self.appid + '.app' if self.appid == 'org.kde.kdeconnect' else self.appid
            self.desktop_file = \
                ApplicationDesktopLoader(appid, [f'{self.xdg_data_dir()}/applications', 'appdata-unmaintained']) \
                .find()
        return self.desktop_file

    def theme(self):
        if not self.icon_theme:
            self.icon_theme = IconTheme('breeze', [f'{self.xdg_data_dir()}/icons'])
        return self.icon_theme

    def is_desktop_app(self):
        # is type desktop or desktop-application
        return self.appdata['Type'].startswith('desktop')

    # FIXME: overridden for git crawling
    def grab_icon(self):
        if not self.is_desktop_app():
            if 'Icon' in self.appdata and 'stock' in self.appdata['Icon']:
                icon_name = self.appdata['Icon']['stock']
            else:  # addon for now
                icon_name = self.appdata.get('Icon', 'folder-build')
        elif os.path.basename(self.project.id) in type(self)._unmaintained:
            icon_name = 'planetkde'  # a generic icon
        else:
            icon_name = self.get_desktop_file().icon
        return None if not icon_name else IconFetcher(icon_name, self.theme()).extend_appdata(self.appdata, self.appid)

    def grab_categories(self):
        categories = [] if 'Categories' not in self.appdata else self.appdata['Categories']
        # Special cases: khelpcenter
        if self.appid in ['org.kde.khelpcenter']:
            categories = ['System']
        # If the categories are defined in both desktop and appdata files, combine them
        if self.is_desktop_app():
            categories += self.get_desktop_file().categories
            if not categories:
                raise CollectorException(f'{self.appid} has no category')
        # Make sure to filter out all !main categories.
        subcategories = list(set(categories) & set(category.MAIN_SUBCATEGORIES))
        self.appdata['subcategories'] = [category.to_code(x) for x in subcategories]
        categories = list(set(categories) & set(category.MAIN_CATEGORIES))
        self.appdata['Categories'] = [category.to_code(x) for x in categories]

    def grab_generic_name(self):
        if self.is_desktop_app():
            self.appdata['X-KDE-GenericName'] = XMLLanguages.from_desktop_entry(self.get_desktop_file(), 'GenericName')
        else:
            self.appdata['X-KDE-GenericName'] = self.appdata['Summary']

    def grab_project(self):
        self.appdata['X-KDE-Project'] = self.project.id
        self.appdata['X-KDE-Repository'] = self.project.repo

    def grab(self):
        if self.appdata.get('Type', 'generic') not in self.APPLICATION_TYPES:
            logging.warning(f'{self.appid} is not an application')
            return False
        # addons have no desktop files
        if self.is_desktop_app():
            if not self.get_desktop_file():
                raise CollectorException(f'No desktop file for {self.appid}')
            if not self.get_desktop_file().show_in('KDE'):
                raise CollectorException(f'Desktop file for {self.appid} is not meant for display')

        logging.info(f'Now processing ID: {self.appdata["ID"]}')
        # thumbnails are not used anymore
        self.grab_icon()
        self.grab_categories()
        self.grab_generic_name()
        self.grab_project()

        os.makedirs('../appdata', exist_ok=True)
        filename = self.appdata['ID'].replace('.desktop', '')
        with open(f'../appdata/{filename}.json', 'w') as f_json:
            json.dump(self.appdata, f_json, ensure_ascii=False, indent=2)

        # FIXME: we should put EVERYTHING into a well defined tree in a tmpdir,
        #   then move it into place in ONE place. so we can easily change where
        #   stuff ends up in the end and know where it is while we are working on
        #   the data
        return True

    @classmethod
    def grab_data(cls, directory, project: Project, theme=None):
        # FIXME: the return value is no good. we need to differentiate:
        #    found no appdata from no good appdata from good appdata
        #    former would need subsequent collectors run, latter simply means the
        #    project contains nothing worthwhile
        if os.path.basename(project.id) in cls._unmaintained:
            k_id = f'org.kde.{os.path.basename(project.id)}'
            dirs = glob.glob(f'appdata-unmaintained/{k_id}.appdata.xml')
        else:
            # metainfo.xml is the right extension to use unless you're an
            # app and don't want to use that one in which case use appdata.xml
            dirs = glob.glob(f'{directory}/**/**.appdata.xml', recursive=True)
            dirs += glob.glob(f'{directory}/**/**.metainfo.xml', recursive=True)
        for d in dirs:
            if 'org.example' in d:
                dirs.remove(d)
        any_good = False
        for path in dirs:
            logging.info(f'Grabbing {path}')
            try:
                good = cls(directory, path, project, theme).grab()
                if not any_good:
                    any_good = good
            except CollectorException as e:
                logging.warning(e)
        return any_good


class GitAppstreamCollector(AppstreamCollector):
    def xdg_data_dir(self):
        return f'{os.getcwd()}/breeze-icons/share'

    # FIXME: deferring to appstream via xdg_data_dir
    # def grab_icon
    #   raise 'not implemented'
    #   # a) should look in breeze-icon unpack via theme
    #   # b) should try to find in tree?
    #   # c) maybe an override system?
    # end

    def get_desktop_file(self):
        if self.desktop_file:
            return self.desktop_file
        if os.path.basename(self.project.id) in type(self)._unmaintained:
            k_id = f'org.kde.{os.path.basename(self.project.id)}'
            files = glob.glob(f'appdata-unmaintained/{k_id}.deskto')
        else:
            files = glob.glob(f'{self.directory}/**/{self.appid}.desktop', recursive=True)
            files += glob.glob(f'{self.directory}/**/{self.appid}.desktop.in', recursive=True)
        for f in files:
            if 'snap/setup' in f or 'snap/gui' in f or 'APPNAMELC' in f or 'org.example' in f:
                files.remove(f)
        if len(files) != 1:
            raise Exception(f'{self.appid}.desktop: {repr(files)}')
        logging.info(f'Found desktop at {files[0]}')
        self.desktop_file = Desktop(files[0])
        return self.desktop_file
